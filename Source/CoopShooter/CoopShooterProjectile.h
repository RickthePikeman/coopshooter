// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoopShooterProjectile.generated.h"

UCLASS(config=Game)
class ACoopShooterProjectile : public AActor
{
	GENERATED_BODY()

#pragma region Components
	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

public:

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

#pragma endregion

#pragma region Stats
protected:
	//The damage type and damage that will be done by this projectile
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Damage")
		TSubclassOf<class UDamageType> DamageType;

	//The damage dealt by this projectile.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Damage")
		float Damage;

#pragma endregion

#pragma region Effects
protected:
	// Particle used when the projectile impacts against another object and explodes.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Effects")
		class UParticleSystem* ExplosionEffect;

#pragma endregion

public:

	ACoopShooterProjectile();

protected:
	virtual void Destroyed() override;

	/** called when projectile hits something */
	UFUNCTION(Category = "Projectile")
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};