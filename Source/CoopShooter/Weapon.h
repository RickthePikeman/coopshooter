// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class COOPSHOOTER_API AWeapon : public AActor
{
	GENERATED_BODY()

#pragma region Components

protected:
	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USkeletalMeshComponent* FP_Gun;

public:
	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(BlueprintReadOnly, Category = Mesh)
		class USceneComponent* FP_MuzzleLocation;
	
#pragma endregion

//These may all need to be modifiable later on by other classes. MAY
#pragma region Stats
protected:

	/** Projectile class to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		TSubclassOf<class ACoopShooterProjectile> ProjectileClass;

	//This may be exposed to a player at somepoint and having an round number rather than a decimal is easier to a player to think about
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float FireRateStat = 50.0f;

	/** Delay between shots in seconds. Used to control fire rate for our test projectile, but also to prevent an overflow of server functions from binding SpawnProjectile directly to input.*/
	float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float HipSpread = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float ADSSpread = 1.0f;

	bool AimingDownSights = false;

	UFUNCTION(BlueprintPure, Category = "Aiming")
		FORCEINLINE bool GetAdsState() const { return AimingDownSights; }

	UFUNCTION(BlueprintCallable, Category = "Aiming")
		void SetAdsState(bool newState) { AimingDownSights = newState; }

	UFUNCTION(BlueprintImplementableEvent, Category = "Stats")
		FRotator SetSpread();

#pragma endregion

#pragma region Effects

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* FireSound;

	UFUNCTION(Unreliable, NetMulticast)
		void DoWeaponEffect();

#pragma endregion


public:	
	// Sets default values for this actor's properties
	AWeapon();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Category = "Gameplay")
		void TickAuthority(float DeltaTime);

	UFUNCTION(Category = "Gameplay")
		void SpawnProjectile();

	/** Property replication */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

#pragma region Firing Properties

protected:
	/** If true, we are in the process of firing projectiles. */
	bool isFiring = false;

	float FiringTimer = 0.0f;

	UFUNCTION(Server, Reliable, withValidation)
		void ServerStartFire();

	UFUNCTION(Server, Reliable, withValidation)
		void ServerEndFire();
public:

	UFUNCTION(BlueprintCallable)
		void StartFire();

	UFUNCTION(BlueprintCallable)
		void EndFire();

#pragma endregion
};