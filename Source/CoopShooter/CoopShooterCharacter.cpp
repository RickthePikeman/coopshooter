// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CoopShooterCharacter.h"
#include "CoopShooterProjectile.h"
#include "CoopShooterHUD.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"

#include "Animation/AnimInstance.h"

#include "GameFramework/InputSettings.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "Engine/World.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ACoopShooterCharacter

ACoopShooterCharacter::ACoopShooterCharacter()
{
	bReplicates = true;
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	//Mesh1P->SetOwnerNoSee
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));	

	Mesh1C = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1C"));
	Mesh1C->SetOwnerNoSee(true);
	Mesh1C->bCastDynamicShadow = false;
	Mesh1C->CastShadow = true;
	Mesh1C->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	Mesh1C->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	Mesh1C->SetupAttachment(RootComponent);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetupAttachment(RootComponent);

	HandPoint = CreateDefaultSubobject<USceneComponent>(TEXT("HandPoint"));
	HandPoint->SetupAttachment(RootComponent);
	HandPoint->SetRelativeLocation(FVector(0.0f, 40.0f, 20.0f));
	HandPoint->SetRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	//Initialize the player's Health
	MaxHealth = 100.0f;
	CurrentHealth = MaxHealth;
}

void ACoopShooterCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	Mesh1P->SetHiddenInGame(false, true);
}

#pragma region Health

float ACoopShooterCharacter::TakeDamage(float DamageTaken, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	ChangeHealth(DamageTaken, MathOp::subtract);

	return CurrentHealth;
}


void ACoopShooterCharacter::ChangeHealth(float healthChange, MathOp operation)
{
	if (HasAuthority())
	{
		float newCurrentHealth = 0;

		switch (operation)
		{
		case MathOp::add:
			newCurrentHealth = CurrentHealth + healthChange; break;
		case MathOp::subtract:
			newCurrentHealth = CurrentHealth - healthChange; break;
		case MathOp::multiply:
			newCurrentHealth = CurrentHealth * healthChange; break;
		case MathOp::divide:
			newCurrentHealth = CurrentHealth / healthChange; break;
		case MathOp::equal:
			newCurrentHealth = MaxHealth; break;
		}
		CurrentHealth = FMath::Clamp(newCurrentHealth, 0.0f, MaxHealth);

		//FString bindingMessage = FString::Printf(TEXT("New value of health %f"), CurrentHealth);
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, bindingMessage);

		OnHealthChanged();
	}
}

void ACoopShooterCharacter::OnHealthUpdate()
{
	if (IsLocallyControlled() == true)
	{
		/*FString bindingMessage = FString::Printf(TEXT("Health Bar being updated"));
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, bindingMessage);*/
		UpdateHealthBar();
		if (CurrentHealth <= 0)
		{
			OnDeath();
		}
	}
}

void ACoopShooterCharacter::OnHealthChanged()
{
	if (IsLocallyControlled())
	{
		UpdateHealthBar();
	}
}

#pragma endregion

#pragma region Weapon

void ACoopShooterCharacter::SetCurrentWeapon(TSubclassOf<AWeapon> weapon)
{
	if (this->GetInstigator()->IsLocallyControlled()) {
		ServerSetWeapon(weapon);
	}
}

void ACoopShooterCharacter::ServerSetWeapon_Implementation(TSubclassOf<AWeapon> weapon)
{
	SetWeapon(weapon);
}

void ACoopShooterCharacter::SetWeapon(TSubclassOf<AWeapon> weapon)
{
	if (IsValid(CurrentWeapon)) {
		CurrentWeapon->Destroy();
	}

	FTransform spawnTransform = HandPoint->GetRelativeTransform();

	FActorSpawnParameters spawnParameters;
	spawnParameters.Instigator = this;
	spawnParameters.Owner = this;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;	

	CurrentWeapon = GetWorld()->SpawnActor<AWeapon>(weapon, spawnTransform, spawnParameters);

	FAttachmentTransformRules transformRules(EAttachmentRule::KeepRelative, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, true);

	CurrentWeapon->AttachToComponent(HandPoint, transformRules);

	if (IsValid(CurrentWeapon)) {
		FString debugMessage = FString::Printf(TEXT("Weapon made for player: %s"), *this->GetDebugName(this));
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);
	}
}

void ACoopShooterCharacter::TryStartFire()
{
	if (IsValid(CurrentWeapon))
	{
		
		CurrentWeapon->StartFire();
	}
	else 
	{
		//Player has no weapon
		FString debugMessage = FString::Printf(TEXT("No valid weapon for player: %s"), *this->GetDebugName(this));
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);
	}
}

void ACoopShooterCharacter::TryEndFire()
{
	if (IsValid(CurrentWeapon))
	{
		CurrentWeapon->EndFire();
	}
}

#pragma endregion

#pragma region Death & Revival

void ACoopShooterCharacter::OnDeath()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	this->DisableInput(PlayerController);

	UWorld* World = GetWorld();
	World->GetTimerManager().SetTimer(RevivalTimer, this, &ACoopShooterCharacter::Revive, respawnTimer, false);
}

void ACoopShooterCharacter::Revive()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	this->EnableInput(PlayerController);

	ChangeHealth(MaxHealth, MathOp::equal);
}

#pragma endregion

#pragma region Movement

void ACoopShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	//PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ACoopShooterCharacter::StartFire);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ACoopShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACoopShooterCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACoopShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACoopShooterCharacter::LookUpAtRate);
}

void ACoopShooterCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ACoopShooterCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ACoopShooterCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACoopShooterCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

#pragma endregion

#pragma region Replicated Properties

void ACoopShooterCharacter::OnRep_CurrentHealth()
{
	OnHealthUpdate();
}

void ACoopShooterCharacter::OnRep_CurrentWeapon()
{
	/*FString debugMessage = FString::Printf(TEXT("Checking weapon state"));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);*/
	if (false) 
	{
		if (IsLocallyControlled())
		{
			if (IsValid(CurrentWeapon))
			{
				FString debugMessage = FString::Printf(TEXT("Local weapon has been created for player: %s"), *this->GetDebugName(this));
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);
			}
			else
			{
				//Player has no weapon
				FString debugMessage = FString::Printf(TEXT("No valid local weapon for player: %s"), *this->GetDebugName(this));
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);
			}
		}
		if (GetLocalRole() == ROLE_Authority)
		{
			if (IsValid(CurrentWeapon))
			{
				FString debugMessage = FString::Printf(TEXT("Remote weapon has been created for player: %s"), *this->GetDebugName(this));
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);
			}
			else
			{
				//Player has no weapon
				FString debugMessage = FString::Printf(TEXT("No valid remote weapon for player: %s"), *this->GetDebugName(this));
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, debugMessage);
			}
		}
	}
}

void ACoopShooterCharacter::GetLifetimeReplicatedProps(TArray <FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(float, CurrentHealth);
	DOREPLIFETIME(ACoopShooterCharacter, CurrentHealth);
	//DOREPLIFETIME(ACoopShooterCharacter, CurrentWeapon);
	DOREPLIFETIME_CONDITION_NOTIFY(ACoopShooterCharacter, CurrentWeapon, COND_None, REPNOTIFY_Always);
}

#pragma endregion