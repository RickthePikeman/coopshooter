// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
//#include "Blueprint/UserWidget.h"

#include "CoopShooterHUD.generated.h"

UCLASS()
class ACoopShooterHUD : public AHUD
{
	GENERATED_BODY()

public:
	ACoopShooterHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	//UPROPERTY(EditAnywhere, Category = "Pause")
		//TSubclassOf<UUserWidget> pauseMenuRef;

	//UUserWidget* pauseInstance;

	//UFUNCTION(BlueprintCallable)
	//	void OpenPauseMenu();

	//UFUNCTION(BlueprintCallable)
	//	void ClosePauseMenu();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Graphics")
	class UTexture2D* CrosshairTex;
};

