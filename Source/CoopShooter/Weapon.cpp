// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"
#include "CoopShooterProjectile.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"

#include "Animation/AnimInstance.h"

#include "GameFramework/InputSettings.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "Engine/World.h"

// Sets default values
AWeapon::AWeapon()
{
	bReplicates = true;

	PrimaryActorTick.bCanEverTick = true;

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetCollisionProfileName(TEXT("NoCollision"));
	FP_Gun->SetRelativeRotation(FQuat(0.0f, 0.0f, -90.0f, 0.0f));
	FP_Gun->SetupAttachment(RootComponent);
	

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.0f, 60.0f, 10.0f));
	FP_MuzzleLocation->SetRelativeRotation(FQuat(0.0f, 0.0f, 90.0f, 0.0f));

	ProjectileClass = ACoopShooterProjectile::StaticClass();

	FireRate = 10 / FireRateStat;
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority()) 
	{
		TickAuthority(DeltaTime);
	}
	
}

void AWeapon::TickAuthority(float DeltaTime)
{
	if (FiringTimer > 0)
	{
		FiringTimer = FiringTimer - DeltaTime;
	}

	if (isFiring && FiringTimer <= 0)
	{
		SpawnProjectile();
		FiringTimer = FireRate;
		DoWeaponEffect();
	}
}

void AWeapon::SpawnProjectile()
{
	FRotator SpawnRotation = SetSpread();
	//FP_MuzzleLocation->GetComponentLocation() + (FP_MuzzleLocation->GetRightVector() * 20.0f);
	const FVector SpawnLocation = FP_MuzzleLocation->GetComponentLocation() + (FP_MuzzleLocation->GetRightVector() * 20.0f);
	
	FActorSpawnParameters spawnParameters;
	spawnParameters.Instigator = GetInstigator();
	spawnParameters.Owner = GetOwner();
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	UWorld* const World = GetWorld();
	World->SpawnActor<ACoopShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, spawnParameters);
}

void AWeapon::GetLifetimeReplicatedProps(TArray <FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

#pragma region Effects

void AWeapon::DoWeaponEffect_Implementation()
{
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
}

#pragma endregion

#pragma region Firing Properties

void AWeapon::StartFire()
{
	ServerStartFire();
}

void AWeapon::EndFire()
{
	ServerEndFire();
}

void AWeapon::ServerStartFire_Implementation()
{
	isFiring = true;
}

void AWeapon::ServerEndFire_Implementation()
{
	isFiring = false;
}

bool AWeapon::ServerStartFire_Validate()
{
	return true;
}

bool AWeapon::ServerEndFire_Validate()
{
	return true;
}

#pragma endregion