// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CoopShooterProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/DamageType.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Net/UnrealNetwork.h"

#include "Engine/Engine.h"

ACoopShooterProjectile::ACoopShooterProjectile() 
{
	bReplicates = true;

	//Definition for the SphereComponent that will serve as the Root component for the projectile and its collision.
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	
	//Registering the Projectile Impact function on a Hit event.
	if (HasAuthority())
	{
		CollisionComp->OnComponentHit.AddDynamic(this, &ACoopShooterProjectile::OnHit);
	}
	RootComponent = CollisionComp;

	//FString bindingMessage = FString::Printf(TEXT("Effect name: %s"), *ExplosionEffect->GetFullName());
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, bindingMessage);

	//Definition for the Projectile Movement Component.
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 1500.0f;
	ProjectileMovement->MaxSpeed = 1500.0f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->ProjectileGravityScale = 0.0f;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	DamageType = UDamageType::StaticClass();
	Damage = 10.0f;
}

void ACoopShooterProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (HasAuthority())
	{
		

		// Only add impulse and destroy projectile if we hit a physics
		if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
		{
			OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
		}

		else if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL)) {

			if (OtherActor)
			{
				UGameplayStatics::ApplyPointDamage(OtherActor, Damage, NormalImpulse, Hit, GetInstigator()->Controller, this, DamageType);
			}
		}
		Destroy();
	}
}

void ACoopShooterProjectile::Destroyed()
{
	FVector spawnLocation = GetActorLocation();
	UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionEffect, spawnLocation, FRotator::ZeroRotator, true, EPSCPoolMethod::AutoRelease);
}