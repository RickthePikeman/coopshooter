// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CoopShooterHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"

#include "Engine/Engine.h"
#include "Engine/World.h"

ACoopShooterHUD::ACoopShooterHUD()
{
	// Set the crosshair texture
	//static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/Textures/FirstPersonCrosshair"));
	//CrosshairTex = CrosshairTexObj.Object;

	//pauseInstance = CreateWidget<UUserWidget>(GetWorld(), pauseMenuRef);
}


void ACoopShooterHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	if (CrosshairTex != NULL) {
		FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}

//void ACoopShooterHUD::OpenPauseMenu()
//{
//	FString deathMessage = FString::Printf(TEXT("Paused	"));
//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, deathMessage);
//
//	//UUserWidget* pauseInstance = CreateWidget<UUserWidget>(GetWorld(), pauseMenuRef);
//	//pauseInstance->AddToViewport();
//}
//
//void ACoopShooterHUD::ClosePauseMenu()
//{
//	FString deathMessage = FString::Printf(TEXT("unPaused"));
//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, deathMessage);
//
//	//pauseInstance->RemoveFromParent();
//}