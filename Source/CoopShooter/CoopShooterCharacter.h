// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapon.h"
#include "CoopShooterCharacter.generated.h"

UENUM(BlueprintType)
enum class MathOp : uint8
{
	add UMETA(DisplayName = "Add"),
	subtract UMETA(DisplayName = "Subtract"),
	multiply UMETA(DisplayName = "Multiply"),
	divide UMETA(DisplayName = "Divide"),
	equal UMETA(DisplayName = "Equal")
};

class UInputComponent;

UCLASS(config = Game)
class ACoopShooterCharacter : public ACharacter
{
private:
	GENERATED_BODY()

#pragma region Components

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USkeletalMeshComponent* FP_Gun;

	/* Character mesh that other players will see */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USkeletalMeshComponent* Mesh1C;

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleAnywhere, Category = Mesh)
		class USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCameraComponent;

public:

	//The handpoint is set ot be visible anywhere so the transform can be reset in the editor
	//Unreal seems to refuse any updates to the transform from c++, so resetting in editor seems like the only option
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		class USceneComponent* HandPoint;

	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }

	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1C() const { return Mesh1C; }

	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimMontage* FireAnimation;

#pragma endregion

public:
	ACoopShooterCharacter();

protected:
	virtual void BeginPlay();

#pragma region Health
public:
	/** Event for taking damage. Overridden from APawn.*/
	float TakeDamage(float DamageTaken, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
protected:
	/** The player's current health. When reduced to 0, they are considered dead.*/
	UPROPERTY(ReplicatedUsing = OnRep_CurrentHealth)
		float CurrentHealth;

	/** Getter for Current Health.*/
	UFUNCTION(BlueprintPure, Category = "Health")
		FORCEINLINE float GetCurrentHealth() const { return CurrentHealth; }

	/** The player's maximum health. This is the highest that their health can be, and the value that their health starts at when spawned.*/
	UPROPERTY(EditDefaultsOnly, Category = "Health")
		float MaxHealth;

	/** Getter for Max Health.*/
	UFUNCTION(BlueprintPure, Category = "Health")
		FORCEINLINE float GetMaxHealth() const { return MaxHealth; }

	UFUNCTION(BlueprintPure, Category = "Health")
		FORCEINLINE float GetHealthPercent() { return CurrentHealth / MaxHealth; }

	//This is the health setter for ANY kind of health change
	UFUNCTION(BlueprintCallable, Category = "Health")
		void ChangeHealth(float healthChange, MathOp operation);

	/** Response to health being updated. Called on the server immediately after modification, and on clients in response to a RepNotify*/
	void OnHealthUpdate();

	//For any functionalty that needs to happen after health has changed
	UFUNCTION()
		void OnHealthChanged();

	//A blueprint implemented version for any functionalty that needs to happen after health has changed
	UFUNCTION(BlueprintImplementableEvent, Category = "Health")
		void UpdateHealthBar();
#pragma endregion

#pragma region Weapon
protected:
	UPROPERTY(ReplicatedUsing = OnRep_CurrentWeapon)
		AWeapon* CurrentWeapon;

	UFUNCTION(BlueprintPure, Category = "Weapon")
		FORCEINLINE AWeapon* GetCurrentWeapon() const { return CurrentWeapon; }

	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void SetCurrentWeapon(TSubclassOf<AWeapon> weapon);

	UFUNCTION(Server, reliable , Category = "Weapon")
		void ServerSetWeapon(TSubclassOf<AWeapon> weapon);
	
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void SetWeapon(TSubclassOf<AWeapon> weapon);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void TryStartFire();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void TryEndFire();

#pragma endregion

#pragma region Death & Revival
protected:

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		void OnDeath();

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		void Revive();

	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
		float respawnTimer = 3.0;

	// Timer for tracking player respawn
	FTimerHandle RevivalTimer;

#pragma endregion

#pragma region Movement

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, sin deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

protected:
	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	void TurnAtRate(float Rate);

	void LookUpAtRate(float Rate);

	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

#pragma endregion

#pragma region Replicated Properties

protected:
	/** RepNotify for changes made to current health.*/
	UFUNCTION()
		void OnRep_CurrentHealth();

	UFUNCTION()
		void OnRep_CurrentWeapon();
public:
	/** Property replication */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

#pragma endregion
};